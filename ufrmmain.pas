unit uFrmMain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ActnList, ComCtrls,
  Menus, ExtCtrls, StdCtrls, DividerBevel, BCPanel;

type

  { TFrmMain }

  TFrmMain = class(TForm)
    ActionKlijentParams: TAction;
    ActionQuit: TAction;
    ActionListMain: TActionList;
    DividerBevel1: TDividerBevel;
    ImagePascal: TImage;
    LabelStatusKlijent: TLabel;
    LabelStatus: TLabel;
    LabelTitle: TLabel;
    PanelStatus: TPanel;
    PanelForms: TBCPanel;
    ImageList16png: TImageList;
    MainMenuApp: TMainMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    N2: TMenuItem;
    N1: TMenuItem;
    PanelMenu: TPanel;
    ProgressBarZaSve: TProgressBar;
    Splitter1: TSplitter;
    TimerStopTitle: TTimer;
    TimerStartTitle: TTimer;
    ToolBarMain: TToolBar;
    ToolButton1: TToolButton;
    procedure ActionKlijentParamsExecute(Sender: TObject);
    procedure ActionQuitExecute(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: boolean);
    procedure FormShow(Sender: TObject);
    procedure TimerStartTitleTimer(Sender: TObject);
    procedure TimerStopTitleTimer(Sender: TObject);
  private

  public

  end;

var
  FrmMain: TFrmMain;

implementation
uses
  uStringConst, UMainUtilities, uDlgKlijentParams, uKlijentParam;
{$R *.lfm}

{ TFrmMain }

procedure TFrmMain.ActionQuitExecute(Sender: TObject);
begin
  // prosto zatvori app
  Close;
end;

procedure TFrmMain.ActionKlijentParamsExecute(Sender: TObject);
const
  SUCCESS_MSG : String = 'Podaci su uspešno sačuvani.';
  //WRONG_PASSWORD_MSG : String = 'Neispravna lozinka za pristup.';
var
  Dlg : TDlgKlijentParams;
  DatPath : String;
  ErrorMsg : String;
  //DlgPassword : TDlgSimplePassword;
begin
  (* ************** ne za sada **************************************************
     // najpre proveri ima li privilegije
   DlgPassword:= TDlgSimplePassword.Create(nil);
   if not(DlgPassword.ShowModal = mrOk) then
   begin
     DlgPassword.Free;
     Exit;
   end
   else
   begin
     if not(DlgPassword.EditPassword.Text = UDBcommon.ADMIN_PASSWORD) then
     begin
       ShowMessage(WRONG_PASSWORD_MSG);
       DlgPassword.Free;
       Exit;
     end;
   end;
   *****************************************************************************)
   ErrorMsg:= ''; // za sada nema greske
   Dlg:= TDlgKlijentParams.Create(nil);
   if(Dlg.ShowModal = mrOk) then
   begin
    // dodeli vrednosti i sacuvaj sve
    Dlg.FDBKlijent.LocalHost:= ShortString(Trim(LeftStr(Dlg.EditLocalHost.Text, 20)));
    Dlg.FDBKlijent.AltHost:= ShortString(Trim(LeftStr(Dlg.EditAltHost.Text, 20)));
    Dlg.FDBKlijent.RemoteHost:= ShortString(Trim(LeftStr(Dlg.EditRemoteHost.Text, 20)));
    // ako je ista uneo, dodeli inace se podrazumeva da je na mestu gde i izvrsna app
    if(Trim(LeftStr(Dlg.EditLibPath.Text, 200)) <> '') then
      Dlg.FDBKlijent.LibPath:= ShortString(Trim(LeftStr(Dlg.EditLibPath.Text, 200)));   // posto je fillchar onda nema potrebe za alternativu
    Dlg.FDBKlijent.DbProtocol:= ShortString(Trim(LeftStr(Dlg.CmbDbProtokol.Items[Dlg.CmbDbProtokol.ItemIndex], 30)));
    Dlg.FDBKlijent.DbPort:= ShortString(Trim(LeftStr(Dlg.EditDbPort.Text, 5)));
    // ako je uneo bilo sta
    if (Trim(LeftStr(Dlg.EditLocalDb.Text, 200)) <> '') then
    begin
      if Pos('Firebir', Dlg.CmbDbProtokol.Text) <> 0 then
         Dlg.FDBKlijent.DbLocalDirPath:= ShortString(IncludeTrailingPathDelimiter(Trim(LeftStr(Dlg.EditLocalDb.Text, 200)))) // za firbird
      else
         Dlg.FDBKlijent.DbLocalDirPath:= ShortString(Trim(LeftStr(Dlg.EditLocalDb.Text, 200)));
    end;
    if Trim(LeftStr(Dlg.EditAltDb.Text, 200)) <> '' then
    begin
      if Pos('Firebir', Dlg.CmbDbProtokol.Text) <> 0 then
        Dlg.FDBKlijent.DbAltDirPath:= ShortString(IncludeTrailingPathDelimiter(Trim(LeftStr(Dlg.EditAltDb.Text, 200))))
      else
        Dlg.FDBKlijent.DbAltDirPath:= ShortString(Trim(LeftStr(Dlg.EditAltDb.Text, 200)));
    end;
    if (Trim(LeftStr(Dlg.EditRemoteDB.Text, 200)) <> '') then
    begin
      if Pos('Firebir', Dlg.CmbDbProtokol.Text) <> 0 then
        Dlg.FDBKlijent.DbRemoteDirPath:= ShortString(IncludeTrailingPathDelimiter(Trim(LeftStr(Dlg.EditRemoteDB.Text, 200))))
      else
        Dlg.FDBKlijent.DbRemoteDirPath:= ShortString(Trim(LeftStr(Dlg.EditRemoteDB.Text, 200)));
    end;
    Dlg.FDBKlijent.DefaultUser:= ShortString(Trim(LeftStr(Dlg.CmbDefaultUser.Text, 15)));   // text posto nisam siguran za Items.Count?
    Dlg.FDBKlijent.DefaultDB:= ShortString(Trim(LeftStr(Dlg.EditDefaultDB.Text, 20)));
    Dlg.FDBKlijent.DefaultHost:= Dlg.CmbDefaultHost.ItemIndex;
    // izracunaj putanju gde se cuva file(app-path)  a sam dodaje naziv datoteke
    DatPath:= IncludeTrailingPathDelimiter(ExtractFilePath(ParamStr(0)));
    (* debug msg...
    ShowMessage('Putanja koja je izracunata: ' + DatPath);
    ********* end debug msg ******************************)
    try
      if uKlijentParam.WriteTKlijentDBSettings(Dlg.FDBKlijent, DatPath, ErrorMsg) then
      begin
        ShowMessage(SUCCESS_MSG);
      end else
      begin
        ShowMessage(ErrorMsg);
      end;
    except
      on E : Exception do
      begin
        ShowMessage(E.Message);
        Dlg.Free;
        Exit;
      end;
    end;
   end;
   // free dialog
   Dlg.Free;
end;

procedure TFrmMain.FormCloseQuery(Sender: TObject; var CanClose: boolean);
begin
  // probaj da disable-ujes timer-e
  if(self.TimerStopTitle.Enabled) then
  begin
    try
      self.TimerStopTitle.Enabled:= False;
    except
      // pa i nije bitno
    end;
  end;
  // sledeci je start timer za zaustavljanje
  if(self.TimerStartTitle.Enabled) then
  begin
    try
      self.TimerStartTitle.Enabled:= False;
    except
      // pa i nije bitno
    end;
  end;
  // kako god mora da izadje
  CanClose:= True;
end;

procedure TFrmMain.FormShow(Sender: TObject);

begin
  // za pocetak prokreni timer za prikaz poruke
  self.LabelTitle.Caption:= ''; // da zapocne poruku
  self.TimerStartTitle.Enabled:= True;

  // prikazi datum u status-u
  self.LabelStatusKlijent.Caption:= UMainUtilities.PrikaziDanasnjiDatum();
end;

procedure TFrmMain.TimerStartTitleTimer(Sender: TObject);
begin
  //ako je bio pokrenut stop timer, zaustavi ga
  if(TimerStopTitle.Enabled) then
    TimerStopTitle.Enabled:= False;
  // prikazivanje poruke app
  case Length(LabelTitle.Caption) of
    0   :  LabelTitle.Caption:= 'K';
    1   :  LabelTitle.Caption:= LabelTitle.Caption + 'o';
    2   :  LabelTitle.Caption:= LabelTitle.Caption + 'n';
    3   :  LabelTitle.Caption:= LabelTitle.Caption + 't';
    4   :  LabelTitle.Caption:= LabelTitle.Caption + 'r';
    5   :  LabelTitle.Caption:= LabelTitle.Caption + 'o';
    6   :  LabelTitle.Caption:= LabelTitle.Caption + 'l';
    7   :  LabelTitle.Caption:= LabelTitle.Caption + 'a';
    8   :  LabelTitle.Caption:= LabelTitle.Caption + ' ';
    9   :  LabelTitle.Caption:= LabelTitle.Caption + 'r';
    10  :  LabelTitle.Caption:= LabelTitle.Caption + 'e';
    11  :  LabelTitle.Caption:= LabelTitle.Caption + 'v';
    12  :  LabelTitle.Caption:= LabelTitle.Caption + 'i';
    13  :  LabelTitle.Caption:= LabelTitle.Caption + 'z';
    14  :  LabelTitle.Caption:= LabelTitle.Caption + 'i';
    15  :  LabelTitle.Caption:= LabelTitle.Caption + 'j';
    16  :  LabelTitle.Caption:= LabelTitle.Caption + 'a';
    17  :  LabelTitle.Caption:= LabelTitle.Caption + ' ';
    18  :  LabelTitle.Caption:= LabelTitle.Caption + 'i';
    19  :  LabelTitle.Caption:= LabelTitle.Caption + 'z';
    20  :  LabelTitle.Caption:= LabelTitle.Caption + 'v';
    21  :  LabelTitle.Caption:= LabelTitle.Caption + 'o';
    22  :  LabelTitle.Caption:= LabelTitle.Caption + 'r';
    23  :  LabelTitle.Caption:= LabelTitle.Caption + 'n';
    24  :  LabelTitle.Caption:= LabelTitle.Caption + 'o';
    25  :  LabelTitle.Caption:= LabelTitle.Caption + 'g';
    26  :  LabelTitle.Caption:= LabelTitle.Caption + ' ';
    27  :  LabelTitle.Caption:= LabelTitle.Caption + 'k';
    28  :  LabelTitle.Caption:= LabelTitle.Caption + 'o';
    29  :  LabelTitle.Caption:= LabelTitle.Caption + 'd';
    30  :  LabelTitle.Caption:= LabelTitle.Caption + 'a';
    31  :  LabelTitle.Caption:= LabelTitle.Caption + ' ';
    32  :  LabelTitle.Caption:= LabelTitle.Caption + 'P';
    33  :  LabelTitle.Caption:= LabelTitle.Caption + 'a';
    34  :  LabelTitle.Caption:= LabelTitle.Caption + 's';
    35  :  LabelTitle.Caption:= LabelTitle.Caption + 'c';
    36  :  LabelTitle.Caption:= LabelTitle.Caption + 'a';
    37  :  LabelTitle.Caption:= LabelTitle.Caption + 'l';
    38  :  LabelTitle.Caption:= LabelTitle.Caption + ' ';
    39  :  LabelTitle.Caption:= LabelTitle.Caption + '.';
    40  :  LabelTitle.Caption:= LabelTitle.Caption + '.';
    41  :  LabelTitle.Caption:= LabelTitle.Caption + '.';
    42  :  LabelTitle.Caption:= LabelTitle.Caption + ' ';
    43  :  LabelTitle.Caption:= LabelTitle.Caption + uStringConst.CHECK_MARK;
    else
      begin
        TimerStartTitle.Enabled:= False;
        TimerStopTitle.Enabled:= True;
      end;
  end;
  LabelTitle.Update;
  Application.ProcessMessages;
end;

procedure TFrmMain.TimerStopTitleTimer(Sender: TObject);
begin
  // disable timer start (mada ovo nije moguce)
  if(TimerStartTitle.Enabled) then
    TimerStartTitle.Enabled:= False;
  // vrati poruku na staro (prazno)
  LabelTitle.Caption:= '';
  Application.ProcessMessages;
  TimerStartTitle.Enabled:= True;
end;

end.

