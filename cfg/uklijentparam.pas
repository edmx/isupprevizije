unit uKlijentParam;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

{$Define RAZVOJ}

  const
    (* Default putanja gde se nalazi app zbog zapisivanja datoteka(constans)
       Ovo je sada samo neka putanja a uglvnom ce se koristiti path app *)
    DEFAULT_APP_PATH      : String = 'c:\EdxProduction\';
    // Default ime datoteke u koju se zapisuju podaci za DB na klijentskom racunaru(constans = KP.dat)
    DEFAULT_DAT_KLIJENTDB : String = 'KP.dat';
    {$IfDef RAZVOJ}
      ADMIN_PASSWORD      : String = '';
    {$EndIf}
    {$IfDef DEMO}
      ADMIN_PASSWORD      : String = 'Morar013';
    {$EndIf}
    {$IfDef PRODUKCIJA}
      ADMIN_PASSWORD      : String = 'Csharp013';
    {$EndIf}
    DEMO_DB_USER          : String = 'root';
    DEMO_DB_PASSWORD      : String = 'Csharp013';
    DEFAULT_DB_EXTENSION  : STring = '';
    RAZVOJ_DB_USER        : String = 'root';
    RAZVOJ_DB_PASSWORD    : String = 'Csharp013';

  type

    (* Record za upotrebu na klijentskom racunaru
       Packed record koji sadrzi sve sto je potrebno
       za konekciju sa bazom(bazama) i treba u binarnom obliku da
       se cuva na klijentskom racunaru.
       Packed zbog cuvanja na tvrdom disku  *)
    TKlijentParams = packed record
      (* Adresa servera na lokalnoj mrezi *)
      LocalHost  : String[20];
      (* Remote server kada se koristi internet konekcija *)
      RemoteHost : String[20];
      (* Alternativna adresa servera na lokalnoj mrezi koja se koristi
         kada glavni nije dostupan, moze biti replika ili nekakva kopija baze za
         alternativnu upotrebu  *)
      AltHost    : String[20];
      (* Putanja do *.dll biblioteke za konkretnu bazu(protokol) *)
      LibPath    : String[200];
      (* Protokol(firebird-2.5 npr.) koji se koristi za konekciju sa bazom podataka  *)
      DbProtocol : String[30];
      (* U slucaju da ima vise instanci servera port se mora razlikovati *)
      DbPort     : String[5];
      (* Default putanja(do folder-a) gde su smestene baze podataka
         na lokalnom serveru, ukoliko je tipa Firebird ili slican
         koja se mapira pomocu putanje(kao dbf npr.) *)
      DbLocalDirPath : String [200];
      (* Default putanja(do folder-a) gde su smestene baze podataka
         na alternativnom lokalnom serveru. *)
      DbAltDirPath   : String [200];
      (* Default putanja(do folder-a) gde su smestene baze podataka
         na remote serveru. *)
      DbRemoteDirPath : String [200];
      (* Default user, kako bi iz liste korisnika predvidjenih za konkretni
         racunar kako bi korisniku bio predlozen kao prvi u ComboBox-u  *)
      DefaultUser     : String[15];
      (* Default DB pri izboru iz padajuce liste *)
      DefaultDB       : String[20];
      (* Default host koji se nudi kod logovanja
         Ovo je bitno i kod visekorisnickog okruzenja, zbog default izbora *)
      DefaultHost     : Byte;
      (* Rezerva byte-ova za prosirenje polja po potrebi *)
      Rezerva         : Array[0..900] of Byte;
      (* velizina nesto manja od 2048 byte-s, taman za citanje u "dahu" *)
    end;

  // Metod koji cu sam odrzavati prema promenama biblioteke
  (* zeoslib biblioteke a vraca listu podrzanih protokola
     parametar "NewList" prosledjuje se po referenci i prikuplja
     podatke o podrzanim protokolima
     Nije neko sretno resenje a sluzi da bih mogao da na svakom
     klijenstkom racunaru postavim binarnu datoteku sa predefinisanim
     parametrima za konekciju, prema potrebi *)
  function GetDbProtocolList(var NewList : TStringList) : Boolean;

  (* Funkcija koja vraca boolean = true ako je uspesno zapisan record
     tipa  TKlijentParams. Ovo koristi administrator na lokalnom klijentskom racunaru
     za svaki terminal. ErrorMsg se prosledjuje po referenci da bi se uhvatile greske.
     Overload *)
  function WriteTKlijentDBSettings(NewSettings : TKlijentParams; var ErrorMsg : String) : Boolean; overload;
  // Funkcija koja vraca boolean = true ako je uspesno zapisan record
  //  tipa  TKlijentDB. Ovo koristi administrator na lokalnom klijentskom racunaru
  //  za svaki terminal. ErrorMsg se prosledjuje po referenci da bi se uhvatile greske.
  //   Overload, u ovom obliku prihvata path i sam dodaje naziv datoteke
  function WriteTKlijentDBSettings(NewSettings : TKlijentParams; CurrPath : String; var ErrorMsg : String) : Boolean; overload;
  // Vrati default putanju(dir) aplikacije
  function GetDefaultAppPath() : String;
  // Vrati default naziv datoteke tipa TKlijentDB
  function GetDefaultNameOfKlijentDat() : String;
  // Citanje datoteke tipa TKlijentDB  gde se po referenci prosledjuju
  //  objekat tipa(record) i string koji treba da prikupi poruku greske
  function ReadTKlijentDBdat(var CurrRecord : TKlijentParams; var ErrorMsg : String; RecordPath : String) : Boolean;

implementation

function GetDbProtocolList(var NewList: TStringList): Boolean;
var
  Success : Boolean;  //return
begin
  Success:= False;
  try
    NewList.Clear; // ako je slucajno bilo podataka
    NewList.Add('ado');
    NewList.Add('asa');
    NewList.Add('ASA12');
    NewList.Add('ASA7');
    NewList.Add('ASA8');
    NewList.Add('ASA9');
    NewList.Add('firebird');
    NewList.Add('firebird-1.0');
    NewList.Add('firebird-1.5');
    NewList.Add('firebird-2.0');
    NewList.Add('firebird-2.1');
    NewList.Add('firebird-2.5');
    NewList.Add('firebird-3.0');
    NewList.Add('FreeTDS_MsSQL<=6.5');
    NewList.Add('FreeTDS_MsSQL>=2005');
    NewList.Add('FreeTDS_MsSQL-2000');
    NewList.Add('FreeTDS_MsSQL-7.0');
    NewList.Add('FreeTDS_Sybase<10');
    NewList.Add('FreeTDS_Sybase-10+');
    NewList.Add('interbase');
    NewList.Add('interbase-6');
    NewList.Add('MariaDB-10');
    NewList.Add('MariaDB-5');
    NewList.Add('mssql');
    NewList.Add('mysql');
    NewList.Add('mysql-4.1');
    NewList.Add('mysql-5');
    NewList.Add('mysqld-4.1');
    NewList.Add('mysqld-5');
    NewList.Add('oracle');
    NewList.Add('oracle-9.i');
    NewList.Add('pooled.*');
    NewList.Add('postgresql');
    NewList.Add('postgresql-7');
    NewList.Add('postgresql-8');
    NewList.Add('postgresql-9');
    NewList.Add('sqlite');
    NewList.Add('sqlite-3');
    NewList.Add('sybase');
  except
    Result:= Success;
    Exit;
  end;

  // sve proslo
  Success:= True;
  Result:= Success;
end;

function WriteTKlijentDBSettings(NewSettings: TKlijentParams; var ErrorMsg: String): Boolean;
var
  MyDat    : file  of TKlijentParams;
  FullPath : String;
  Success  : Boolean;
begin
  Success:= False;
  // ako nije prosledjeno onda je ovo putanja
  FullPath:= IncludeTrailingPathDelimiter(DEFAULT_APP_PATH) + DEFAULT_DAT_KLIJENTDB;
  try
    AssignFile(MyDat, FullPath);
    Rewrite(MyDat); // prazna datoteka upisuje se uvek iznova
    Write(MyDat, NewSettings);
    // to je to, zatvori
    CloseFile(MyDat);
  except
    on E : Exception do
    begin
      ErrorMsg:= E.Message;
      Result:= Success;
      Exit;
    end;
  end;
  // ako je sve ok vrati true
  Success:= True;
  Result:= Success;
end;

function WriteTKlijentDBSettings(NewSettings: TKlijentParams; CurrPath: String; var ErrorMsg: String): Boolean;
var
  MyDat    : file  of TKlijentParams;
  Success  : Boolean;
begin
  Success:= False;
  // ako nije zavrsen sa "\" sada jeste
  CurrPath:= IncludeTrailingPathDelimiter(CurrPath);
  // dodaj i ime datoteke
  CurrPath:= CurrPath + DEFAULT_DAT_KLIJENTDB;
  try
    AssignFile(MyDat, CurrPath);
    Rewrite(MyDat); // prazna datoteka upisuje se uvek iznova
    Write(MyDat, NewSettings);
    // to je to, zatvori
    CloseFile(MyDat);
  except
    on E : Exception do
    begin
      ErrorMsg:= E.Message;
      Result:= Success;
      Exit;
    end;
  end;
  // ako je sve ok vrati true
  Success:= True;
  Result:= Success;
end;

function GetDefaultAppPath(): String;
begin
  Result:= DEFAULT_APP_PATH;
end;

function GetDefaultNameOfKlijentDat(): String;
begin
  Result:= DEFAULT_DAT_KLIJENTDB;
end;

function ReadTKlijentDBdat(var CurrRecord: TKlijentParams; var ErrorMsg: String;RecordPath: String): Boolean;
var
  Success : Boolean;
  DbDatFile : file of TKlijentParams;
begin

  Success:= False; // nesuspesno
  ErrorMsg:= ''; // po potrebi prikupice greske
  try
    // procitaj podatke iz datoteke
    AssignFile(DbDatFile, RecordPath);
    Reset(DbDatFile);
    Read(DbDatFile, CurrRecord);
    CloseFile(DbDatFile);
  except
    on E : Exception do
    begin
      ErrorMsg:= E.Message; // poruka izuzetka
      Result:= Success;
      Exit;
    end;
  end;

  // ako je sve u redu
  Success:= True;
  Result:= Success;
end;

end.

