unit uDlgKlijentParams;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtCtrls,
  Buttons, uColorConst, BCButtonFocus, uKlijentParam, UMainUtilities ;

type

  { TDlgKlijentParams }

  TDlgKlijentParams = class(TForm)
    BtnCancel: TBCButtonFocus;
    BtnOk: TBCButtonFocus;
    BtnSearchLocalPath: TSpeedButton;
    CmbDbProtokol: TComboBox;
    CmbDefaultUser: TComboBox;
    CmbDefaultHost: TComboBox;
    EditRemoteDb: TEdit;
    EditDefaultDB: TEdit;
    EditLibPath: TEdit;
    EditLocalDB: TEdit;
    EditAltDb: TEdit;
    EditLocalHost: TEdit;
    EditAltHost: TEdit;
    EditRemoteHost: TEdit;
    EditDbPort: TEdit;
    GroupBox1: TGroupBox;
    ImageHandPointer: TImage;
    Label1: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    LabelErrorMsg: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    PanelDb: TPanel;
    BtnSearchLibPath: TSpeedButton;
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnOkClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    (* Putanja do datoteke gde je sacuvan record *)
    FDBdatPath : String;
  public
    { Public declarations }

    (* Record u kom ce se cuvati podaci(ucitavati) i koji se na kraju
       cuva na tvrdom disku za stalnu upotrebu
       Public jer koristim kao dijalog *)
    FDBKlijent :  TKlijentParams;

    (* procedura za postavljanje inicijalne boje editora.fonta *)
    procedure InitEditore();
  published
    { Published declration }

    (* Procedura za postavljanje hand-pointer image kada je kontrola u focusu *)
    procedure CtrlOnFocus(Sender : TObject);

    (* Procedura za ukidanje hand-pointer image kada je kontrola van focusa *)
    procedure CtrlLostFocus(Sender : TObject);
  end;

var
  DlgKlijentParams: TDlgKlijentParams;

implementation
uses
  LCLType;
{$R *.lfm}

{ TDlgKlijentParams }

procedure TDlgKlijentParams.FormCreate(Sender: TObject);
var
  CurrPath : String = '';
  TmpDbKlijent : TKlijentParams; // ako datoteka postoji, ucitaj u ovaj record
  RecordPath : String = '';
  DbDatPostoji : Boolean = False;
  ListaProtokola : TStringList;
  I : Byte; // za popunjavanje ComboBox-a
  ErrorMsg : String = ''; // za prikupljanje gresaka iz funkcije u UDBcommon
begin
  (* koristim predefinisanu boju za formulare
     ovo nije bilo neophodno ali ce ovako stojati i
     u drugim formama, da bih mogao da koristim "theme" *)
  self.Color:= uColorConst.FORM_DEFAULT_COLOR;

  // za sada nema greske kod citanja datoteke
  self.LabelErrorMsg.Caption:= '';
  //za postojecu datoteku sa podacima
  DbDatPostoji:= False;
  FDBdatPath:= '';
  // izracunaj trenutnu putanju(app)
  CurrPath:= ParamStr(0);

  // nisam siguran za inicijalizaciju ovog tipa ali probacu
  Initialize(TmpDbKlijent); (* dobro radi i samo jedan poziv je u pitanju, tako da je ok *)
  // listu protokola mogu da popunim odmah a ako postoji default, pronaci cu je po index-u
  try
    self.CmbDbProtokol.Items.Clear;
    ListaProtokola:= TStringList.Create;
    uKlijentParam.GetDbProtocolList(ListaProtokola);
    for I := 0 to ListaProtokola.Count - 1 do
    begin
      self.CmbDbProtokol.Items.Add(ListaProtokola[I]);
    end;
  finally
    ListaProtokola.Free; // oslobodi listu
  end;
  //inicijalizuj record koji ces da cuvas
  FillChar(FDBKlijent, SizeOf(TKlijentParams), #0);
  // proveri da li podaci postoje
  RecordPath:= IncludeTrailingPathDelimiter(ExtractFilePath(CurrPath)) + uKlijentParam.GetDefaultNameOfKlijentDat();
  if FileExists(RecordPath) then
  begin
    FDBdatPath:= RecordPath;
    DbDatPostoji:= True;
  end
  else
  begin
    RecordPath:= uKlijentParam.GetDefaultAppPath() + uKlijentParam.DEFAULT_DAT_KLIJENTDB;
    if FileExists(RecordPath) then
    begin
      FDBdatPath:= RecordPath;
      DbDatPostoji:= True;
    end;
  end;

  // ucitavanje postojecih podataka
  FillChar(TmpDbKlijent, SizeOf(TKlijentParams), #0);
  if DbDatPostoji then
  begin
    if uKlijentParam.ReadTKlijentDBdat(TmpDbKlijent, ErrorMsg, RecordPath) then
    begin
      // dodeli ucitane vrednosti
      self.EditLocalHost.Text:= String(TmpDbKlijent.LocalHost);
      self.EditAltHost.Text:= String(TmpDbKlijent.AltHost);
      self.EditRemoteHost.Text:= String(TmpDbKlijent.RemoteHost);
      self.EditDbPort.Text:= String(TmpDbKlijent.DbPort);
      self.EditLibPath.Text:= String(TmpDbKlijent.LibPath);
      self.EditLocalDb.Text:= String(TmpDbKlijent.DbLocalDirPath);
      self.EditAltDb.Text:= String(TmpDbKlijent.DbAltDirPath);
      self.EditRemoteDB.Text:= String(TmpDbKlijent.DbRemoteDirPath);
      self.EditDefaultDB.Text:= String(TmpDbKlijent.DefaultDB);
      // pronadji u comboBox-u default user-a ako ga ima
      try
        if (self.CmbDefaultUser.Items.Count > 0) then
          self.CmbDefaultUser.ItemIndex:= self.CmbDefaultUser.Items.IndexOf(String(TmpDbKlijent.DefaultUser))
        else
          self.CmbDefaultUser.Text:=  String(TmpDbKlijent.DefaultUser);
      except
        self.CmbDefaultUser.ItemIndex:= -1; // nema izabranog
      end;
      // pronadji u comboBox-u protokol ako ga ima
      try
        if (self.CmbDbProtokol.Items.Count > 0) then
          self.CmbDbProtokol.ItemIndex:= self.CmbDbProtokol.Items.IndexOf(String(TmpDbKlijent.DbProtocol));
      except
        self.CmbDbProtokol.ItemIndex:= -1; // nema izabranog
      end;
    end else
    begin
      // prikazi grsku da znas sta je poslo naopako kod citanja datoteke
      self.LabelErrorMsg.Caption:= 'Error read .dat: ' + ErrorMsg;
    end;
    // na osnovu indexa, prikazi default host
    try
      self.CmbDefaultHost.ItemIndex:= TmpDbKlijent.DefaultHost;
    except
      self.CmbDefaultHost.ItemIndex:= -1;
    end;
  end;
  // inicijalizuj boje i font editora
  self.InitEditore();
end;

procedure TDlgKlijentParams.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  (* za ok shift + return *)
  if (ssCtrl in Shift) and (Key = VK_RETURN) then
  begin
    ModalResult:= mrOK;
  end;
  // ostali
  case Key of
    VK_RETURN:
      begin
        (* kretanje na enter *)
        Key:= 0;
        self.SelectNext(TForm(self).ActiveControl, True, True);
      end;
    VK_ESCAPE:
      begin
        ModalResult:= mrCancel;
      end;
  end;
end;

procedure TDlgKlijentParams.InitEditore();
var
  I : Integer;
  CurrCtrl : TWinControl;
begin
  // prelistaj kontrole i promeni boju za sve koje nisu u fokusu
  for I := 0 to Pred(self.ComponentCount) do
  begin
    if (not(self.Components[I] = self.ActiveControl)) and (self.Components[I] is TWinControl) then
    begin
      CurrCtrl:= TWinControl(self.Components[I]);
      // za TEdit
      if (CurrCtrl is TEdit) then
      begin
        CurrCtrl.Color:= uColorConst.EDITOR_COLOR_BROWSE;
        TEdit(CurrCtrl).Font.Color:= uColorConst.EDITOR_BROWSE_FONT_COLOR;
      end;
      // za TComboBox
      if (CurrCtrl is TComboBox) then
      begin
        CurrCtrl.Color:= uColorConst.EDITOR_COLOR_BROWSE;
        TComboBox(CurrCtrl).Font.Color:= EDITOR_BROWSE_FONT_COLOR;
      end;
    end;
  end;
end;

procedure TDlgKlijentParams.CtrlOnFocus(Sender: TObject);
begin
  // mislim da mi ovo resava pitanje prmestanja po tab-ovima i groupBox-ovima etc...
  ImageHandPointer.Parent:= TWinControl(Sender).Parent;
  ImageHandPointer.Left:=  TWinControl(Sender).Left - 20 ;
  ImageHandPointer.Top:=  TWinControl(Sender).Top + 5 ;
  ImageHandPointer.Visible:= True;
  // promeni boju
  UMainUtilities.EditorOnFocus(TWinControl(Sender));
end;

procedure TDlgKlijentParams.CtrlLostFocus(Sender: TObject);
begin
 ImageHandPointer.Visible:= False;
 // promeni boju
 UMainUtilities.EditorLostFocus(TWinControl(Sender));
end;

procedure TDlgKlijentParams.BtnCancelClick(Sender: TObject);
begin
  // vrati cancel
  ModalResult:= mrCancel;
end;

procedure TDlgKlijentParams.BtnOkClick(Sender: TObject);
begin
  // modalResult = Ok
  ModalResult:= mrOK;
end;

end.

