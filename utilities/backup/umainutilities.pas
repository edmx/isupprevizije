unit UMainUtilities;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, StdCtrls, Graphics, uColorConst, Controls;

(* Funkcija koja vraca naziv dana u nedelji na srpskom i sam datum kao jedinu bitnu
   DateTime informaciju koju cu prikazivati *)
function PrikaziDanasnjiDatum() : String;

(* Procedura koja menja boju editora (Edit, DBEdit, Cmb i sl. kada je kontrola
   u fokusu *)
procedure EditorOnFocus(var Ctrl : TWinControl);

(* Procedura koja menja boju editora (Edit, DBEdit, Cmb i sl. kada kontrola
   nije u fokusu *)
procedure EditorLostFocus(var Ctrl : TWinControl);

implementation

function PrikaziDanasnjiDatum(): String;
var
  DanUnedelji : Byte;
  DatumMsg, NazivDana   : String;
begin
  // vazan je datum, vreme necu da pokazujem
  DanUnedelji:= DayOfWeek(Now);
  case DanUnedelji of
    1: NazivDana:= 'Nedelja';
    2: NazivDana:= 'Ponedeljak';
    3: NazivDana:= 'Utorak';
    4: NazivDana:= 'Sreda';
    5: NazivDana:= 'Četvrtak';
    6: NazivDana:= 'Petak';
    else
      NazivDana:= 'Subota';
  end;
  DatumMsg:= NazivDana + ' : ' + FormatDateTime('dd.MM.yyyy', Now) + ' god.';
  Result:= DatumMsg;
end;

procedure EditorOnFocus(var Ctrl : TWinControl);
begin
  Ctrl.Color:= clWhite;
  if (Ctrl is TEdit) then
  begin
    TEdit(Ctrl).Font.Color:= clBlack;
  end
  else if (Ctrl is TComboBox) then
  begin
    TComboBox(Ctrl).Font.Color:= clBlack;
  end;
end;

procedure EditorLostFocus(var Ctrl: TWinControl);
begin
  Ctrl.Color:= uColorConst.EDITOR_COLOR_BROWSE;
  if (Ctrl is TEdit) then
  begin
    TEdit(Ctrl).Font.Color:= clWhite;
  end
  else if (Ctrl is TComboBox) then
  begin
    TComboBox(Ctrl).Font.Color:= clWhite;
  end;
end;

end.

