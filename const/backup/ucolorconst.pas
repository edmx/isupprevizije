unit uColorConst;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Graphics;

{$Define DARK_THEME}

const
  {$IfDef DARK_THEME}
    (* boja forme na dark pozadini (svidja mi se ova tamnija) *)
    FORM_DEFAULT_COLOR : TColor = TColor($00444444); // bledja je ova($00585858);

    (* boja fonta na dark pozadini *)
    LABEL_FONT_COLOR : TColor = clCream;

    (* boja fonta za editor koji je aktivan *)
    EDITOR_ACTIVE_FONT_COLOR : TColor = clBlack;

    EDITOR_BROWSE_FONT_COLOR : TColor = clCream;

    (* boja editora kada nije u fikusu *)
    EDITOR_COLOR_BROWSE : TColor = TColor($002A2A2A); //mora tamnije od forme;
  {$EndIf}

implementation

end.

